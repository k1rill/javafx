package sample;

import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import sample.student.Student;
import sample.student.StudentsList;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public Button okButton;
    public Label label;
    public ListView<Student> listView;
    public TextField course;
    public TextField lastName;
    public TextField firstName;
    public TextField middleName;
    public Button saveButton;
    private StudentsList list;
    private final static String FILENAME = "saved.txt";

    private StudentsList getList() {
        if (list == null) {
            list = new StudentsList();
        }
        return list;
    }

    public void onAddClick(ActionEvent actionEvent) {
        list.addStudent(
                new Student(
                        Integer.parseInt(course.getText()),
                        lastName.getText(),
                        firstName.getText(),
                        middleName.getText()
                )
        );
        saveButton.setDisable(false);
        refresh();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObjectMapper mapper = new ObjectMapper();
        List<Student> list = new ArrayList<>();
        try {
            list = mapper.readValue(new File(FILENAME),
                    new com.fasterxml.jackson.core.type.TypeReference<List<Student>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Student student: list) {
            getList().addStudent(student);
        }
        saveButton.setDisable(true);
        refresh();
    }

    public void onSaveClick(ActionEvent actionEvent) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File(FILENAME), getList().getStudentsList());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void refresh() {
        listView.setItems(
                FXCollections.observableList(getList().getStudentsList()));
    }
}
