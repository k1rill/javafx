package sample.student;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Student implements Serializable {
    private int course;
    private String lastName;
    private String firstName;
    private String secondName;

    Student() {
    }

    public Student(int course, String lastName, String firstName, String secondName) {
        this.course = course;
        this.lastName = lastName;
        this.firstName = firstName;
        this.secondName = secondName;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    @JsonIgnore
    public String getFullName() {
        return lastName + " " + firstName + " " + secondName;
    }

    @JsonIgnore
    public int getNameLength() {
        return lastName.length() + firstName.length() + secondName.length();
    }

    @JsonIgnore
    public String getInitials() {
        return lastName.substring(0, 1) + "." + firstName.substring(0, 1) + "." + secondName.substring(0, 1) + ".";
    }

    @JsonIgnore
    public String getNumberOfCharacters() {
        char[] arr = getFullName().toCharArray();

        Map<Character, Integer> map = new HashMap<>();

        for (char c : arr) {
            int count = 0;
            for (char anArr : arr) {
                if (c == anArr) {
                    count++;
                }
            }
            if (count > 1) {
                map.put(c, count);
            }
        }

        return map.toString();
    }

    @Override
    public String toString() {
        return "Курс: " + course +
                ", ФИО: " + getFullName();
    }
}
