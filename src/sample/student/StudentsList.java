package sample.student;

import java.util.ArrayList;
import java.util.List;

public class StudentsList {
    private List<Student> studentsList
            = new ArrayList<>();

    public void addStudent(Student student) {
        studentsList.add(student);
    }

    public List<Student> getStudentsList() {
        return studentsList;
    }

    @Override
    public String toString() {
        String result = "";
        for (Student student: studentsList) {
            result += student + "\n";
        }
        return result;
    }
}
